#ifndef _CONNECTION_H
#define _CONNECTION_H

#include <seastar/net/api.hh>
#include <boost/core/noncopyable.hpp>
#include <boost/intrusive/list.hpp>

class Packet;
class ConnectionImpl;

class Connection : public boost::intrusive::list_base_hook<>, private boost::noncopyable {
public:
	using shutdown_cb = std::function<void(void)>;

	Connection(seastar::connected_socket &&sock, seastar::socket_address &&addr);

	seastar::future<> start();

	void set_on_shutdown(shutdown_cb cb);

	void stop();

	void send_message(seastar::temporary_buffer<char> msg);

	seastar::future<> shutdown();

	bool is_closed() const noexcept;

	~Connection();
private:
	std::unique_ptr<ConnectionImpl> pimpl;
	
};

#endif

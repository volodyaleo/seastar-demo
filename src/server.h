#ifndef _SERVER_H
#define _SERVER_H

#include <memory>
#include <seastar/net/api.hh>
#include <boost/core/noncopyable.hpp>

class ServerImpl;

class Server : private boost::noncopyable {
	std::unique_ptr<ServerImpl> pimpl;
public:
	Server(const seastar::ipv4_addr &addr);
	~Server();

	seastar::future<> start();

	seastar::future<> stop();

	std::size_t client_count();
};

#endif

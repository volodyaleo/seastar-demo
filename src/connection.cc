#include "connection.h"
#include "connection_impl.h"
#include "packet.h"

Connection::Connection(seastar::connected_socket &&sock, seastar::socket_address &&addr)
{
	pimpl = std::make_unique<ConnectionImpl>(std::move(sock), std::move(addr));
}

Connection::~Connection() = default;

seastar::future<> Connection::start()
{
	return pimpl->start();
}

void Connection::stop()
{
	return pimpl->stop();
}

void Connection::set_on_shutdown(shutdown_cb cb)
{
	pimpl->set_on_shutdown(std::move(cb));
}

seastar::future<> Connection::shutdown()
{
	return pimpl->shutdown();
}

void Connection::send_message(seastar::temporary_buffer<char> msg)
{
	pimpl->send_message(std::move(msg));
}

bool Connection::is_closed() const noexcept
{
	return pimpl->is_closed();
}

#ifndef _PACKET_H
#define _PACKET_H

#include <seastar/core/temporary_buffer.hh>
#include <seastar/core/scattered_message.hh>

enum class Command : char {
	CER = 1,
	CEA = 2,
	MESSAGE = 3,
	DWR = 4,
	DWA = 5,
	DPR = 6,
	DPA = 7
};

class Header {
	Command m_cmd;
	std::size_t m_len;
public:
	static constexpr std::size_t HEADER_SIZE = 2;

	explicit Header(const seastar::temporary_buffer<char> &buf);

	Header(Command cmd) : m_len{0}, m_cmd{cmd}
	{

	}

	Header(Command cmd, std::size_t len) :
		m_cmd{cmd}, m_len{len}
	{

	}

	Header(Header &&) = default;
	Header& operator= (Header &&) = default;

	Command get_command() const
	{
		return m_cmd;
	}

	std::size_t get_payload_len() const
	{
		return m_len;
	}

	void set_payload_len(std::size_t len)
	{
		m_len = len;
	}

	seastar::temporary_buffer<char> encode() const;
};

class Packet {
	seastar::temporary_buffer<char> m_payload;
	Header m_header;
public:
	Packet(Header header, seastar::temporary_buffer<char> &&buf) :
		m_header{std::move(header)}, m_payload{std::move(buf)}
	{
		m_header.set_payload_len(std::size(m_payload));
	}

	Packet(Header header) :
		m_header{std::move(header)}
	{
		m_header.set_payload_len(0);
	}

	Packet(Packet &&) = default;
	Packet& operator= (Packet &&) = default;

	seastar::temporary_buffer<char> get_payload()
	{
		return m_payload.share();
	}

	const Header& hdr() const
	{
		return m_header;
	}

	std::size_t size() const
	{
		return Header::HEADER_SIZE + std::size(m_payload);
	}

	seastar::scattered_message<char>  encode();
};

#endif

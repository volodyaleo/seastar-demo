#include "server.h"
#include "server_impl.h"

Server::Server(const seastar::ipv4_addr &addr) :
	pimpl{std::make_unique<ServerImpl>(addr)}
{

}

seastar::future<> Server::start()
{
	return pimpl->start();
}

seastar::future<> Server::stop()
{
	return pimpl->stop();
}

std::size_t Server::client_count()
{
	return pimpl->client_count();
}

Server::~Server() = default;

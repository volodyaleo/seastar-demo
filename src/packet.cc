#include "packet.h"

Header::Header(const seastar::temporary_buffer<char> &buf) :
	m_len{static_cast<std::size_t>(buf[0])}, m_cmd{static_cast<Command>(buf[1])}
{
	
}

seastar::temporary_buffer<char> Header::encode() const
{
	char header_buf[HEADER_SIZE] = {
		static_cast<char>(m_len & 0xff),
		static_cast<std::underlying_type_t<Command>>(m_cmd)
	};

	seastar::temporary_buffer<char> header(header_buf, HEADER_SIZE);

	return header;
}

seastar::scattered_message<char> Packet::encode()
{
	seastar::scattered_message<char> res;

	res.append(m_header.encode());

	if (!m_payload.empty())
		res.append(m_payload.share());

	return res;
}

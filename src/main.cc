
#include <seastar/core/app-template.hh>
#include <seastar/core/distributed.hh>
#include <seastar/core/print.hh>

#include "server.h"


int main(int ac, char** av) {
	seastar::app_template app;

	return app.run(ac, av, [] {
		Server server(seastar::ipv4_addr("0.0.0.0", 8080));

		return server.start();
	});

	return 0;
}

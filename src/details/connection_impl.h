#ifndef _CONNECTION_IMPL_H
#define _CONNECTION_IMPL_H

#include <memory>

#include <seastar/core/timer.hh>
#include <seastar/net/api.hh>
#include <seastar/core/gate.hh>

#include <boost/core/noncopyable.hpp>

#include "packet.h"


namespace Events {
	struct START {};
	struct R_CONN_CER {
		seastar::temporary_buffer<char> cer;
	};
	struct TIMEOUT {};
	struct RCV_CER {};
	struct RCV_CEA {};
	struct PEER_DISC {};
	struct RCV_DPR {};
	struct RCV_DPA {};
	struct RCV_DWR {};
	struct RCV_DWA {};
	struct SEND_MESSAGE {
		seastar::temporary_buffer<char> payload;
	};
	struct RCV_MESSAGE {
		seastar::temporary_buffer<char> payload;
	};
	struct STOP {};
};

class ConnectionImpl : private boost::noncopyable {
public:
	using shutdown_cb = std::function<void(void)>;

	ConnectionImpl(seastar::connected_socket &&sock, seastar::socket_address &&addr);

	seastar::future<> start();

	void stop();
	void send_message(seastar::temporary_buffer<char> &&msg);

	seastar::future<> shutdown();

	void set_on_shutdown(shutdown_cb &&cb) {
		m_on_shutdown = std::move(cb);
	}

	bool is_closed() const noexcept
	{
		return m_gate.is_closed();
	}

	~ConnectionImpl();
private:
	struct fsm_impl;
	struct fsm;

	seastar::connected_socket m_sock;
	seastar::input_stream<char> m_inp;
	seastar::output_stream<char> m_out;
	seastar::socket_address m_addr;
	seastar::timer<seastar::lowres_clock> m_timeout;
	seastar::timer<seastar::lowres_clock> m_pingpong;

	std::unique_ptr<fsm_impl> m_sm;
	seastar::gate m_gate;
	shutdown_cb m_on_shutdown;

	static constexpr int PINGPONG_TIMEOUT_SECONDS = 1;
	static constexpr int PINGPONG_INTERVAL_SECONDS = 5;
	static constexpr int DISCONNECTION_TIMEOUT_SECONDS = 1;
	static constexpr int CER_TIMEOUT_SECONDS = 1;

	seastar::future<Packet> read_pkt();
	void process_pkt(Packet &pkt);
	seastar::future<> read_and_process_pkt();

	void send_pkt(Packet &pkt);
	void send_pkt(Packet &&pkt);

	bool g_accept(const Events::R_CONN_CER& event) const;
	void a_process_cer(const Events::R_CONN_CER& event);
	void a_send_cea(const Events::R_CONN_CER& event);
	void a_send_message(const Events::SEND_MESSAGE &msg);
	void a_process_msg(const Events::RCV_MESSAGE &msg);
	void a_process_dwr();
	void a_send_dwa();
	void a_process_dwa();
	void a_reject();
	void a_send_dpr();
	void a_send_dpa();
	void a_closing_enter();
	void a_open_enter();
	void a_open_exit();
};

#endif

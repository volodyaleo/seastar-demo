#ifndef _SERVER_IMPL_H
#define _SERVER_IMPL_H

#include <vector>
#include <memory>

#include <boost/core/noncopyable.hpp>

#include <seastar/net/api.hh>
#include <seastar/core/gate.hh>
#include <boost/intrusive/list.hpp>

#include "connection.h"

class ServerImpl : private boost::noncopyable {
	boost::intrusive::list<Connection> m_connections;
	seastar::server_socket m_listener;
	seastar::gate m_gate;

	void on_accept(seastar::accept_result ar);

	seastar::future<> do_accept();
public:
	ServerImpl(const seastar::ipv4_addr &addr);
	~ServerImpl();

	seastar::future<> start();

	seastar::future<> stop();

	std::size_t client_count() const
	{
		return std::size(m_connections);
	}
};

#endif

#include "server_impl.h"

#include "connection.h"

#include <seastar/core/reactor.hh>
#include <seastar/core/when_all.hh>
#include <seastar/util/log.hh>
#include <seastar/coroutine/all.hh>

static seastar::logger hlogger("srv");

void ServerImpl::on_accept(seastar::accept_result ar)
{
	auto conn = std::make_shared<Connection>(std::move(ar.connection), std::move(ar.remote_address));

	// Prevent server stop if connection inside list
	// If server will be destructed after stop,
	// connection will try to remove itself from destructed object
	if (!m_gate.try_enter())
		return;

	// Remove connection from vector when disconnected
	// Capture connection shared pointer to prevent destruction
	conn->set_on_shutdown([this, conn] () mutable {
		hlogger.debug("Remove connection from list: ...");

		m_connections.erase(m_connections.iterator_to(*conn));

		// Shared ptr released
		conn.reset();

		m_gate.leave();

		hlogger.debug("Remove connection from list: Done");
	});

	m_connections.push_back(*conn);

	(void)conn->start();
}

seastar::future<> ServerImpl::do_accept()
{
	seastar::accept_result ar;

	try {
		ar = co_await m_listener.accept();
	} catch (const std::system_error &e) {
		// stop is called, ECONNABORTED shall be received
		if (e.code().value() != ECONNABORTED) {
			hlogger.error("accept failed: {}", e);
		}
		co_return;
	} catch (std::exception_ptr ex) {
		hlogger.error("accept failed: {}", ex);
		co_return;
	}

	on_accept(std::move(ar));

	co_return;
}

ServerImpl::ServerImpl(const seastar::ipv4_addr &addr)
{
	seastar::listen_options lo;

	lo.proto = seastar::transport::TCP;
	lo.reuse_address = true;

	m_listener = seastar::server_socket(seastar::listen(seastar::make_ipv4_address(addr), lo));	
}

seastar::future<> ServerImpl::start()
{
	hlogger.debug("Server start");
	
	(void)seastar::try_with_gate(m_gate, [this] () {
		return seastar::keep_doing([this] () {
	            return seastar::try_with_gate(m_gate, [this] {
	                return do_accept();
	            });
	        }).handle_exception_type([](const seastar::gate_closed_exception& e) {});;
	}).handle_exception_type([](const seastar::gate_closed_exception& e) {});

	co_return;
}

seastar::future<> ServerImpl::stop()
{
	hlogger.debug("Stop");

	m_listener.abort_accept();

	for (auto &c : m_connections) {
		(void)c.shutdown();
	}

	co_await m_gate.close();

	co_return;
}

ServerImpl::~ServerImpl() = default;

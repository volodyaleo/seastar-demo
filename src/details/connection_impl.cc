#include "connection_impl.h"

#include <boost/sml.hpp>
#include <seastar/core/when_all.hh>
#include <iostream>
#include <seastar/util/log.hh>
#include <seastar/coroutine/all.hh>

static seastar::logger hlogger("conn");

struct ConnectionImpl::fsm {

public:
	auto operator()() noexcept {
		using namespace boost::ext::sml;

		auto a_conn_cert = [](ConnectionImpl *p_conn, const Events::R_CONN_CER& evt) {
			p_conn->a_process_cer(evt);
			p_conn->a_send_cea(evt);
		};
		auto a_shutdown = [](ConnectionImpl *p_conn) { (void)p_conn->shutdown(); };
		auto g_conn_cer = [](ConnectionImpl *p_conn, const Events::R_CONN_CER& event) {
			return p_conn->g_accept(event);
		};
		auto a_send_message = [](ConnectionImpl *p_conn, const Events::SEND_MESSAGE &msg){
			p_conn->a_send_message(msg);
		};
		auto a_process_msg = [](ConnectionImpl *p_conn, const Events::RCV_MESSAGE &msg) {
			p_conn->a_process_msg(msg);
		};

		auto a_process_dwr = [](ConnectionImpl *p_conn) {
			p_conn->a_process_dwr();
			p_conn->a_send_dwa(); 
		};

		auto a_process_dwa = [](ConnectionImpl *p_conn) {
			p_conn->a_process_dwa();
		};

		auto a_reject = [](ConnectionImpl *p_conn) {
			p_conn->a_reject();
		};

		auto a_send_dpr = [](ConnectionImpl *p_conn) {
			p_conn->a_send_dpr();
		};

		auto a_send_dpa = [](ConnectionImpl *p_conn) {
			p_conn->a_send_dpa();
		};

		auto a_closing_enter = [](ConnectionImpl *p_conn) {
			p_conn->a_closing_enter();
		};

		auto a_open_enter = [](ConnectionImpl *p_conn) {
			p_conn->a_open_enter();
		};

		auto a_open_exit = [](ConnectionImpl *p_conn) {
			p_conn->a_open_exit();
		};

		return make_transition_table(
			*"Closed"_s + event<Events::R_CONN_CER> [g_conn_cer] / a_conn_cert = "Open"_s,
			"Closed"_s + event<Events::PEER_DISC> / a_shutdown = "Closed"_s,
			"Closed"_s + event<Events::TIMEOUT> / a_shutdown = "Closed"_s,

			"Open"_s + on_entry<_> / a_open_enter,
			"Open"_s + event<Events::SEND_MESSAGE> / a_send_message = "Open"_s,
			"Open"_s + event<Events::RCV_MESSAGE> / a_process_msg = "Open"_s,
			"Open"_s + event<Events::RCV_DWR> / a_process_dwr = "Open"_s,
			"Open"_s + event<Events::RCV_DWA> / a_process_dwa = "Open"_s,
			"Open"_s + event<Events::R_CONN_CER> / a_reject = "Open"_s,
			"Open"_s + event<Events::STOP> / a_send_dpr = "Closing"_s,
			"Open"_s + event<Events::RCV_DPR> / a_send_dpa = "Closing"_s,
			"Open"_s + event<Events::PEER_DISC> / a_shutdown = "Closed"_s,
			"Open"_s + event<Events::TIMEOUT> / a_send_dpr = "Closing"_s,
			"Open"_s + boost::ext::sml::on_exit<_> / a_open_exit,

			"Closing"_s + on_entry<_> / a_closing_enter,
			"Closing"_s + event<Events::TIMEOUT> / a_shutdown = "Closed"_s,
			"Closing"_s + event<Events::PEER_DISC> / a_shutdown = "Closed"_s,
			"Closing"_s + event<Events::RCV_DPA> / a_shutdown = "Closed"_s
		);
	}
};

struct ConnectionImpl::fsm_impl {
	boost::ext::sml::sm<ConnectionImpl::fsm> sml;

	fsm_impl(ConnectionImpl *conn) : sml{conn}
	{

	}
};

ConnectionImpl::ConnectionImpl(seastar::connected_socket &&sock, seastar::socket_address &&addr)
		: m_sock{std::move(sock)}, m_addr{std::move(addr)},
	  	  m_inp{m_sock.input()}, m_out{m_sock.output()}
{
	m_sm = std::make_unique<ConnectionImpl::fsm_impl>(this);

	m_timeout.set_callback([&sm = m_sm->sml] {
		hlogger.debug("Timeout");
		sm.process_event(Events::TIMEOUT());
	});
	m_pingpong.set_callback([this]{
		send_pkt(Packet(Header(Command::DWR)));
		m_timeout.arm(std::chrono::seconds(PINGPONG_TIMEOUT_SECONDS));
	});
}

ConnectionImpl::~ConnectionImpl() = default;

seastar::future<> ConnectionImpl::start()
{
	m_sm->sml.process_event(Events::START());
	m_timeout.arm(std::chrono::seconds(CER_TIMEOUT_SECONDS));

	// Outer m_gate prevents class destruction before keep_doing done
	// Othervice, use-after-free with m_gate will be catched at the
	// next loop attempt
	(void)seastar::try_with_gate(m_gate, [this] {
		return seastar::keep_doing([this] {
			return seastar::try_with_gate(m_gate, [this] {
				return read_and_process_pkt();
			});
		});
	}).handle_exception_type([](const seastar::gate_closed_exception& e) {});

	co_return;
}

seastar::future<> ConnectionImpl::shutdown()
{
	hlogger.debug("Shutdown connection");

	if (m_gate.is_closed()) {
		co_return;
	}

	m_timeout.cancel();
	m_pingpong.cancel();

	m_sock.shutdown_input();
	m_sock.shutdown_output();

	co_await m_gate.close();

	if (m_on_shutdown) {
		auto cb{std::move(m_on_shutdown)};

		cb();
	}

	co_return;
}

void ConnectionImpl::stop()
{
	m_sm->sml.process_event(Events::STOP());
}

void ConnectionImpl::send_message(seastar::temporary_buffer<char> &&msg)
{
	m_sm->sml.process_event(Events::SEND_MESSAGE(std::move(msg)));
}

seastar::future<Packet> ConnectionImpl::read_pkt()
{
	auto header_buf = co_await m_inp.read_exactly(Header::HEADER_SIZE);

	if (header_buf.size() < Header::HEADER_SIZE)
		co_return seastar::coroutine::return_exception(std::length_error("Wrong packet payload size"));

	Header hdr(std::move(header_buf));

	auto payload_buf = co_await m_inp.read_exactly(hdr.get_payload_len());

	if (std::size(payload_buf) != hdr.get_payload_len())
		co_return seastar::coroutine::return_exception(std::length_error("Wrong packet payload size"));

	co_return Packet(std::move(hdr), std::move(payload_buf));
}

void ConnectionImpl::process_pkt(Packet &pkt)
{
	hlogger.debug("Pkt received: {}", static_cast<int>(pkt.hdr().get_command()));

	using enum Command;

	switch (pkt.hdr().get_command()) {
	case CER:
		m_sm->sml.process_event(Events::R_CONN_CER(pkt.get_payload()));
		break;
	case DWR:
		m_sm->sml.process_event(Events::RCV_DWR());
		break;
	case DWA:
		m_sm->sml.process_event(Events::RCV_DWA());
		break;
	case MESSAGE:
		m_sm->sml.process_event(Events::RCV_MESSAGE(pkt.get_payload()));
		break;
	case DPR:
		m_sm->sml.process_event(Events::RCV_DPR());
		break;
	case DPA:
		m_sm->sml.process_event(Events::RCV_DPA());
		break;

	default:
		// Ignore unknonw
		break;
	}
}

seastar::future<> ConnectionImpl::read_and_process_pkt()
{
	try {
		Packet pkt = co_await read_pkt();

		process_pkt(pkt);
	} catch (const std::length_error &e) {
		hlogger.debug("Cannot read the packet: {}", e);

		// Socket is broken
		if (m_inp.eof())
			m_sm->sml.process_event(Events::PEER_DISC());
	};

	co_return;
}

void ConnectionImpl::send_pkt(Packet &pkt)
{
	hlogger.debug("Pkt send: {}", static_cast<int>(pkt.hdr().get_command()));

	(void)m_out.write(pkt.encode()).then([this]{
		return m_out.flush();
	});
}

void ConnectionImpl::send_pkt(Packet &&pkt)
{
	hlogger.debug("Pkt send: {}", static_cast<int>(pkt.hdr().get_command()));

	(void)m_out.write(pkt.encode()).then([this]{
		return m_out.flush();
	});
}


bool ConnectionImpl::g_accept(const Events::R_CONN_CER& event) const
{
	if (!event.cer.size())
		return false;
	
	// Validate cer
	return true;
}

void ConnectionImpl::a_process_cer(const Events::R_CONN_CER& event)
{

}

void ConnectionImpl::a_send_cea(const Events::R_CONN_CER& event)
{
	send_pkt(Packet(Header(Command::CEA)));
}

void ConnectionImpl::a_send_message(const Events::SEND_MESSAGE &msg)
{
	send_pkt(Packet(Header(Command::MESSAGE), msg.payload.clone()));
}

void ConnectionImpl::a_process_msg(const Events::RCV_MESSAGE &msg)
{

}

void ConnectionImpl::a_process_dwr()
{

}

void ConnectionImpl::a_send_dwa()
{
	send_pkt(Packet(Header(Command::DWA)));
}

void ConnectionImpl::a_process_dwa()
{
	m_timeout.cancel();
}

void ConnectionImpl::a_reject()
{
	// Do nothing
}

void ConnectionImpl::a_send_dpr()
{
	send_pkt(Packet(Header(Command::DPR)));
}

void ConnectionImpl::a_send_dpa()
{
	send_pkt(Packet(Header(Command::DPA)));
}

void ConnectionImpl::a_open_enter()
{
	m_pingpong.rearm_periodic(std::chrono::seconds(PINGPONG_INTERVAL_SECONDS));
}

void ConnectionImpl::a_open_exit()
{
	m_pingpong.cancel();
}

void ConnectionImpl::a_closing_enter()
{
	m_timeout.cancel();
	m_timeout.arm(std::chrono::seconds(DISCONNECTION_TIMEOUT_SECONDS));
}

# Recomended operation system

Ubuntu 22.04.1 LTS

# Installing dependencies

```bash
git submodule update --init
sudo bash external/seastar/install-dependencies.sh
```

# Project build

## Cmake project configuration

```bash
cmake -S. -Bbuild -G Ninja
```

## Build command

```bash
ninja -C build -j8
```

## Run tests

```bash
ctest --test-dir build/tests
```

## Troubleshooting

Remove build directory to clear cmake cache
```bash
rm -rf ./build
```

#include <gtest/gtest.h>

#include <seastar/core/app-template.hh>
#include <seastar/core/seastar.hh>
#include <seastar/core/with_timeout.hh>
#include <seastar/core/reactor.hh>
#include <seastar/net/tcp.hh>
#include <seastar/net/native-stack.hh>
#include <seastar/net/api.hh>
#include <seastar/core/thread.hh>
#include <seastar/core/gate.hh>
#include <seastar/core/when_all.hh>
#include <seastar/core/sleep.hh>

#include "server.h"
#include "packet.h"

static seastar::ipv4_addr m_addr("127.0.0.1", 8081);
static std::unique_ptr<Server> m_srv;

/**
 *	@brief	Run function inside seastar context
 */
static auto run_on_seastar(auto f)
{
	return seastar::make_ready_future().then(std::move(f)).get();
}

class ConnectionTest : public ::testing::Test {
protected:
	void SetUp() override {
		run_on_seastar([] () -> seastar::future<> {
			m_srv.reset(new Server(m_addr));
			co_await m_srv->start();

			co_return;
		});
	}

	void TearDown() override {
		run_on_seastar([] () -> seastar::future<> {
			if (!m_srv)
				co_return;

			co_await m_srv->stop();

			m_srv.reset();

			co_return;
		});
	}	
};

class SockWrapper {
	seastar::connected_socket m_sock;
	seastar::output_stream<char> out;
	seastar::input_stream<char> inp;

	SockWrapper(seastar::connected_socket &&sock) :
		m_sock{std::move(sock)},
		out{m_sock.output()}, inp{m_sock.input()}
	{

	}
public:
	static SockWrapper connect(seastar::ipv4_addr addr)
	{
		auto sock_f = seastar::connect(addr);
		auto sock = sock_f.get();

		return SockWrapper(std::move(sock));
	}

	~SockWrapper()
	{
		m_sock.shutdown_output();
		m_sock.shutdown_input();
	}

	void write(seastar::scattered_message<char> msg)
	{
		out.write(std::move(msg)).get();
		out.flush().get();
	}

	auto read(std::size_t n)
	{
		return seastar::with_timeout(seastar::timer<>::clock::now() + std::chrono::seconds(1),
			inp.read_exactly(n));
	}

	auto read(std::size_t n, int timeout_seconds)
	{
		return seastar::with_timeout(seastar::timer<>::clock::now() + std::chrono::seconds(timeout_seconds),
			inp.read_exactly(n));
	}

	auto read()
	{
		return seastar::with_timeout(seastar::timer<>::clock::now() + std::chrono::seconds(1), inp.read());
	}

	bool eof()
	{
		return inp.eof();
	}
};

TEST_F(ConnectionTest, OpenAndClose) {
	auto cli_sock = SockWrapper::connect(m_addr);
}

TEST_F(ConnectionTest, SendCer) {
	auto cli_sock = SockWrapper::connect(m_addr);

	char cea_payload_raw[] = {1, 2, 3, 4};
	seastar::temporary_buffer<char> cea_payload(cea_payload_raw, sizeof(cea_payload_raw));
	Header header(Command::CER);
	Packet pkt(std::move(header), std::move(cea_payload));

	(void)cli_sock.write(pkt.encode());

	auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header header_resp(cea_head_f.get());
	auto cea_resp_f = cli_sock.read(header_resp.get_payload_len());

	Packet rsp(std::move(header_resp),  cea_resp_f.get());

	ASSERT_EQ(rsp.hdr().get_command(), Command::CEA);
}

TEST_F(ConnectionTest, DWR) {
	auto cli_sock = SockWrapper::connect(m_addr);

	char cea_raw[] = {1, 2, 3, 4};

	Packet cea(Header(Command::CER), seastar::temporary_buffer<char>(cea_raw, sizeof(cea_raw)));

	cli_sock.write(cea.encode());

	auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header header_resp(cea_head_f.get());
	auto cea_resp_f = cli_sock.read(header_resp.get_payload_len());
	cea_resp_f.get();

	Packet dwr(Header(Command::DWR));

	cli_sock.write(dwr.encode());

	auto dwa_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header dwa_hdr(dwa_head_f.get());
	auto dwa_resp_f = cli_sock.read(dwa_hdr.get_payload_len());

	Packet dwa(std::move(dwa_hdr),  dwa_resp_f.get());

	ASSERT_EQ(dwa.hdr().get_command(), Command::DWA);
}

TEST_F(ConnectionTest, ClientDestroyedIfServerStopped) {
	auto cli_sock = SockWrapper::connect(m_addr);

	char cea_raw[] = {1, 2, 3, 4};

	Packet cea(Header(Command::CER), seastar::temporary_buffer<char>(cea_raw, sizeof(cea_raw)));

	cli_sock.write(cea.encode());

	auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header header_resp(cea_head_f.get());
	auto cea_resp_f = cli_sock.read(header_resp.get_payload_len());
	cea_resp_f.get();

	run_on_seastar([] () -> seastar::future<> {
		co_await m_srv->stop();
		m_srv.reset();
		co_return;
	});

	// EOF will be received with delay
	cli_sock.read(1).get();
	ASSERT_TRUE(cli_sock.eof());
}

TEST_F(ConnectionTest, ClientDestroyedIfSocketBroken) {
	{
		auto cli_sock = SockWrapper::connect(m_addr);

		char cea_raw[] = {1, 2, 3, 4};

		Packet cea(Header(Command::CER), seastar::temporary_buffer<char>(cea_raw, sizeof(cea_raw)));

		cli_sock.write(cea.encode());

		auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
		Header header_resp(cea_head_f.get());
		auto cea_resp_f = cli_sock.read(header_resp.get_payload_len());
		cea_resp_f.get();
	}

	// Wait client shutdown
	seastar::sleep(std::chrono::seconds(1)).get();

	auto cli_cnt = run_on_seastar([] () -> seastar::future<std::size_t> {
		co_return m_srv->client_count();
	});

	ASSERT_EQ(cli_cnt, 0);
}

TEST_F(ConnectionTest, ClientDestroyedIfNoPingPong) {
	auto cli_sock = SockWrapper::connect(m_addr);

	char cea_payload_raw[] = {1, 2, 3, 4};
	seastar::temporary_buffer<char> cea_payload(cea_payload_raw, sizeof(cea_payload_raw));
	Header header(Command::CER);
	Packet pkt(std::move(header), std::move(cea_payload));

	(void)cli_sock.write(pkt.encode());

	auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header header_resp(cea_head_f.get());
	ASSERT_EQ(header_resp.get_command(), Command::CEA);
	cli_sock.read(header_resp.get_payload_len()).get();

	seastar::sleep(std::chrono::seconds(8)).get();

	// Read buffers until EOF
	cli_sock.read().get();
	cli_sock.read().get();
	ASSERT_TRUE(cli_sock.eof());
}

TEST_F(ConnectionTest, ClientDestroyedIfNoPingPongWithDpr) {
	auto cli_sock = SockWrapper::connect(m_addr);

	char cea_payload_raw[] = {1, 2, 3, 4};
	seastar::temporary_buffer<char> cea_payload(cea_payload_raw, sizeof(cea_payload_raw));
	Header header(Command::CER);
	Packet pkt(std::move(header), std::move(cea_payload));

	(void)cli_sock.write(pkt.encode());

	auto cea_head_f = cli_sock.read(Header::HEADER_SIZE);
	Header header_resp(cea_head_f.get());
	ASSERT_EQ(header_resp.get_command(), Command::CEA);
	cli_sock.read(header_resp.get_payload_len()).get();

	auto dpr_head_f = cli_sock.read(Header::HEADER_SIZE, 10);
	Header header_dpr(dpr_head_f.get());
	ASSERT_EQ(header_dpr.get_command(), Command::DPR);
	auto dpr_payload = cli_sock.read(header_dpr.get_payload_len()).get();
	Packet dpr_pkt(std::move(header), std::move(dpr_payload));

	Packet dpa_pkt(Header(Command::DPA));

	cli_sock.write(dpa_pkt.encode());

	seastar::sleep(std::chrono::milliseconds(10)).get();

	// EOF will be received with delay
	cli_sock.read(1).get();
	ASSERT_TRUE(cli_sock.eof());
}

TEST_F(ConnectionTest, ClientDestroyedIfNoCER) {
	auto cli_sock = SockWrapper::connect(m_addr);

	seastar::sleep(std::chrono::seconds(2)).get();

	// EOF will be received with delay
	cli_sock.read(1).get();
	ASSERT_TRUE(cli_sock.eof());
}

int main(int argc, char **argv) {
	seastar::app_template app;

	return app.run(argc, argv, [&] {
		seastar::global_logger_registry().set_logger_level("srv", seastar::log_level::debug);
		seastar::global_logger_registry().set_logger_level("conn", seastar::log_level::debug);

		// Seastar does not allow future::get calls and locks to prevent engine deadlock
		auto f = seastar::async([] {
			::testing::InitGoogleTest();

			seastar::engine().exit(RUN_ALL_TESTS());
		});

		return f;
	});
}

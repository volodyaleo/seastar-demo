#include <gtest/gtest.h>

#include <seastar/core/app-template.hh>
#include <seastar/core/seastar.hh>
#include <seastar/core/with_timeout.hh>
#include <seastar/core/reactor.hh>
#include <seastar/net/tcp.hh>
#include <seastar/net/native-stack.hh>
#include <seastar/net/api.hh>
#include <seastar/core/thread.hh>
#include <seastar/core/gate.hh>
#include <seastar/core/when_all.hh>

#include "packet.h"


class PacketTest : public ::testing::Test {
protected:
	void SetUp() override {

	}

	void TearDown() override {

	}	
};


TEST_F(PacketTest, Smoke) {
	char payload_raw[] = {1, 2, 3, 4};
	seastar::temporary_buffer<char> payload(payload_raw, sizeof(payload_raw));

	Packet pkt(Header(Command::CER), std::move(payload));

	ASSERT_EQ(pkt.size(), Header::HEADER_SIZE + sizeof(payload_raw));

	auto encoded = pkt.encode().release();

	auto &hdr_frag = encoded.frag(0);
	
	ASSERT_EQ(hdr_frag.size, Header::HEADER_SIZE);

	Header decoded_hdr(seastar::temporary_buffer<char>(hdr_frag.base, hdr_frag.size));
	
	ASSERT_EQ(decoded_hdr.get_command(), Command::CER);
	ASSERT_EQ(decoded_hdr.get_payload_len(), sizeof(payload_raw));

	auto &pl_frag = encoded.frag(1);

	Packet decoded_pkt(std::move(decoded_hdr), seastar::temporary_buffer<char>(pl_frag.base, pl_frag.size));

	ASSERT_EQ(decoded_pkt.size(), pkt.size());
	ASSERT_EQ(decoded_pkt.hdr().get_payload_len(), pkt.hdr().get_payload_len());
}
